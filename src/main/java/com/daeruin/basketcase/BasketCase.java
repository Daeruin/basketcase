package com.daeruin.basketcase;

import com.daeruin.basketcase.blocks.BlockRegistry;
import com.daeruin.basketcase.config.BasketCaseConfig;
import com.daeruin.basketcase.events.EventHandler;
import com.daeruin.basketcase.items.ItemRegistry;
import com.daeruin.basketcase.network.GuiHandler;
import com.daeruin.basketcase.network.PacketHandler;
import com.daeruin.basketcase.recipes.OreDictRegistry;
import com.daeruin.basketcase.tileentity.TileEntityBasket;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.proxy.IProxy;
import com.daeruin.primallib.util.PrimalCreativeTab;
import com.daeruin.primallib.util.PrimalUtilRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = BasketCase.MODID,
        name = "BasketCase",
        version = "{@version}",
        dependencies = "required-after:primallib",
        acceptedMinecraftVersions = "1.12, 1.12.1, 1.12.2"
)

public class BasketCase
{
    public static final String MODID = "basketcase";

    @SuppressWarnings("CanBeFinal")
    @Mod.Instance
    public static BasketCase INSTANCE = new BasketCase();

    @SidedProxy(modId = MODID, clientSide = "com.daeruin.basketcase.proxy.ClientProxy", serverSide = "com.daeruin.basketcase.proxy.ServerProxy")
    private static IProxy PROXY = null;

    public static IProxy getProxy()
    {
        return PROXY;
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
        PacketHandler.registerPackets();
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
        GameRegistry.registerTileEntity(TileEntityBasket.class, new ResourceLocation("basketcase:basketcase_tile_entity_basket"));
        PROXY.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e)
    {
        if (BasketCaseConfig.allowWoodSpecificBaskets)
        {
            PrimalUtilRecipe.removeCraftingRecipe(PrimalItemRegistry.BARK_STRIPS_GENERIC);
        }

        OreDictRegistry.registerOreDictEntries();
        PrimalCreativeTab.addBlocksToSortOrder(BlockRegistry.getBlocks());
        PrimalCreativeTab.addItemsToSortOrder(ItemRegistry.getItems());
        PROXY.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e)
    {
        MinecraftForge.EVENT_BUS.register(new EventHandler());
        PROXY.postInit();
    }
}

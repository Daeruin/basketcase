package com.daeruin.basketcase.client.gui;

import com.daeruin.basketcase.inventory.ContainerBasket;
import com.daeruin.basketcase.inventory.ItemStackHandlerBasket;
import com.daeruin.basketcase.tileentity.TileEntityBasket;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class GuiBasket extends GuiContainer
{
    private final ResourceLocation GUI_TEXTURE = new ResourceLocation("basketcase:textures/gui/basket.png");
    private final IInventory playerInventory; // The player inventory bound to this GUI
    private final ItemStackHandlerBasket basketInventory;
    private static final int NUM_ROWS = 2; // numSlots must be divisible by this
    private static final int X_GUI_CENTER = 176 / 2; // 176 is the width of the basket GUI - see GuiBasket.xSize
    private static final int Y_START_SLOTS = 17;
    private static final int SLOT_SIZE = 18;

    public GuiBasket(IInventory playerInventory, ItemStackHandlerBasket basketInventory, TileEntityBasket tileEntityBasket)
    {
        super(new ContainerBasket(playerInventory, basketInventory, tileEntityBasket, basketInventory.getSlots()));

        this.xSize = 176;
        this.ySize = 166;
        this.playerInventory = playerInventory;
        this.basketInventory = basketInventory;
    }

    // Draw inventory names
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        // Get translated name of this inventory
        String basketName = basketInventory.getDisplayName().getUnformattedText();
        // Find center of inventory GUI (xSize/2) and subtract half of inventory name
        int nameStart = (this.xSize / 2) - (this.fontRenderer.getStringWidth(basketName) / 2);

        // Draw basket inventory name
        this.fontRenderer.drawString(basketName, nameStart, 6, 4210752);

        String playerInvName = playerInventory.getDisplayName().getUnformattedText();

        // Draw player inventory name
        this.fontRenderer.drawString(playerInvName, 8, this.ySize - 96 + 2, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        // Draw background rectangle
        this.mc.getTextureManager().bindTexture(GUI_TEXTURE);
        int xStartGui = (this.width - this.xSize) / 2; // this.width is width of screen - this.xSize is width of current GUI
        int yStartGui = (this.height - this.ySize) / 2; // this.height is height of screen - this.ySize is height of current GUI
        this.drawTexturedModalRect(xStartGui, yStartGui, 0, 0, this.xSize, this.ySize);

        int numColumns = basketInventory.getSlots() / NUM_ROWS;
        int xStartSlots = xStartGui + X_GUI_CENTER - (numColumns * SLOT_SIZE) / 2;

        // Draw basket slots
        for (int y = 0; y < NUM_ROWS; ++y)
        {
            for (int x = 0; x < numColumns; ++x)
            {
                int xPosSlots = xStartSlots + (x * SLOT_SIZE) - 1; // The -1 helps the slot textures line up with the container slots
                int yPosSlots = yStartGui + Y_START_SLOTS + (y * SLOT_SIZE) - 1; // The -1 helps the slot textures line up with the container slots
                this.drawTexturedModalRect(xPosSlots, yPosSlots, 7, 83, SLOT_SIZE, SLOT_SIZE);
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }
}

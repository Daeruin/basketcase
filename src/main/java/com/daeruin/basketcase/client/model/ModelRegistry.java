package com.daeruin.basketcase.client.model;

import com.daeruin.basketcase.blocks.BlockRegistry;
import com.daeruin.basketcase.items.ItemRegistry;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ModelRegistry
{
    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event)
    {
        PrimalUtilReg.registerBlockModels(BlockRegistry.getBlocks());
        PrimalUtilReg.registerItemModels(ItemRegistry.getItems());
    }
}

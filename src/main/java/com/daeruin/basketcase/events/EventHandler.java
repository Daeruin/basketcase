package com.daeruin.basketcase.events;

import com.daeruin.basketcase.blocks.BlockRegistry;
import com.daeruin.basketcase.blocks.BlockTileEntityBasket;
import com.daeruin.basketcase.client.gui.GuiBasket;
import com.daeruin.basketcase.config.BasketCaseConfig;
import com.daeruin.basketcase.inventory.ContainerBasket;
import com.daeruin.basketcase.inventory.ItemStackHandlerBasket;
import com.daeruin.basketcase.items.ItemBlockBasket;
import com.daeruin.basketcase.items.ItemRegistry;
import com.daeruin.basketcase.network.BasketInventoryUpdate;
import com.daeruin.basketcase.network.PacketHandler;
import com.daeruin.basketcase.util.BasketCaseUtil;
import com.daeruin.primallib.blocks.PrimalBlockRegistry;
import com.daeruin.primallib.items.PrimalItemRegistry;
import com.daeruin.primallib.util.PrimalUtil;
import com.daeruin.primallib.util.PrimalUtilReg;
import com.google.common.collect.ImmutableList;
import net.minecraft.block.Block;
import net.minecraft.block.BlockShulkerBox;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiShulkerBox;
import net.minecraft.client.player.inventory.ContainerLocalMenu;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderTooltipEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.MissingMappings.Mapping;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.List;

import static net.minecraft.client.gui.GuiScreen.isShiftKeyDown;

public class EventHandler
{
    // This tracks the position of the mouse during RenderTooltipEvent.Pre so it's accessible to RenderTooltipEvent.PostText
    private int mouseX;

    // GENERIC TWIGS --> WOOD-SPECIFIC TWIGS

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onHarvestDropsEvent(BlockEvent.HarvestDropsEvent event)
    {
        // If wood-specific twigs are disabled, this method shouldn't change the drops
        // and players will also be unable to craft wood-specific baskets
        if (BasketCaseConfig.allowWoodSpecificBaskets)
        {
            List<ItemStack> drops = event.getDrops(); // Get all drops from this drop event

            for (int i = 0; i < drops.size(); i++) // Iterate through the list of drops
            {
                // Get the current item in the list of drops
                ItemStack currentItemStack = drops.get(i);
                Item currentItem = currentItemStack.getItem();

                // If PrimalLib is dropping a generic twig, replace it with a wood-specific twig
                if (currentItem == PrimalItemRegistry.TWIG_GENERIC)
                {
                    IBlockState blockState = event.getState();
                    Item itemToDrop = BasketCaseUtil.getTwig(blockState); // Defaults to oak twigs if no other twig is determined
                    ItemStack itemStackToDrop = new ItemStack(itemToDrop, currentItemStack.getCount());

                    drops.set(i, itemStackToDrop);
                }
            }
        }
    }

    // BASKETS ONLY GO INTO HOT BAR
    // Also can't move baskets while you're viewing their inventory

    @SubscribeEvent
    public void entityItemPickupEvent(EntityItemPickupEvent event)
    {
        if (BasketCaseConfig.basketsRestrictedToHotBar)
        {
            EntityItem entityItem = event.getItem();
            Item item = entityItem.getItem().getItem();

            if (item instanceof ItemBlockBasket)
            {
                EntityPlayer player = event.getEntityPlayer();

                for (int i = 0; i < 9; ++i)
                {
                    ItemStack stack = player.inventory.getStackInSlot(i);

                    // Found an empty hot bar slot. Allow basket to be picked up.
                    if (stack.isEmpty())
                        return;
                }

                // No empty hot bar slots found. Cancel item pickup.
                event.setCanceled(true);
            }
        }
    }

    /* Using PlayerContainerEvent.Close to detect when container is closed, then search inventory for baskets
     * PlayerContainerEvent.Close is called in EntityPlayerMP.closeContainer()
     * First calls onContainerClosed for whichever container is open.
     * If that happens to be ContainerPlayer, it will:
     *      Call its super: Container.onContainerClosed(), which drops any item on the cursor
     *      Clear the crafting result
     *      Call Container.clearContainer, which puts crafting grid items back into player inventory
     *      (All containers that call Container.clearContainer: ContainerEnchantment, ContainerPlayer, ContainerRepair, and ContainerWorkbench)
     * Finally, this event - search player inventory for illegal baskets and try to put them in hot bar, else drop on ground
     *
     * If slot is part of InventoryPlayer and index is < 9, it's a hot bar slot - everything else is regular inventory or equipment slots
     * Slot 40 is the off hand - allow baskets there
     */
    @SubscribeEvent
    public void onContainerClosed(PlayerContainerEvent.Close event)
    {
        if (BasketCaseConfig.basketsRestrictedToHotBar)
        {
            EntityPlayerMP player = (EntityPlayerMP) event.getEntityPlayer();
            Container playerContainer = player.inventoryContainer;

            for (Slot slot : playerContainer.inventorySlots)
            {
                int slotIndex = slot.getSlotIndex();

                if (slotIndex > 8 && slotIndex != 40) // This slot isn't in the hot bar and isn't in the off hand
                {
                    ItemStack stackInSlot = slot.getStack().copy();

                    if (stackInSlot.getItem() instanceof ItemBlockBasket)
                    {
                        player.dropItem(stackInSlot, false); // Spawns the EntityItem
                        player.inventory.setInventorySlotContents(slotIndex, ItemStack.EMPTY);
                    }
                }
            }
        }
        // Stuff I tried - may be helpful for future code changes

        // Methods for deleting the basket that's being ejected from the inventory

        // They all seemed to work equally well
        // slot.putStack(ItemStack.EMPTY);
        // inventoryForSlot.removeStackFromSlot(slot.getSlotIndex());
        // player.inventory.setInventorySlotContents(slot.getSlotIndex(), ItemStack.EMPTY);
        // player.inventoryContainer.putStackInSlot(slotIndex, ItemStack.EMPTY);
        // container.putStackInSlot(i, ItemStack.EMPTY);

        // Methods for syncing client with server - only EntityPlayerMP.sendContainerToPlayer worked

        // container.detectAndSendChanges();
        // event.getContainer().detectAndSendChanges();
        // event.getEntityPlayer().inventoryContainer.detectAndSendChanges();
        // player.inventoryContainer.detectAndSendChanges();
        // player.inventory.markDirty();
        // player.sendContainerToPlayer(player.inventoryContainer);
        // event.getEntityPlayer().openContainer.detectAndSendChanges(); // This is only supposed to send the slots that have changed
        // event.getEntityPlayer().inventory.markDirty();
        // inventoryForSlot.markDirty();
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void guiMouse(GuiScreenEvent.MouseInputEvent.Pre event)
    {
        if (Mouse.getEventButton() == -1) // Not interested in mouse movement
        {
            return;
        }

        GuiScreen gui = event.getGui();
        boolean shouldCancel = false;

        if (gui instanceof GuiContainer)
        {
            Slot slotUnderMouse = ((GuiContainer) gui).getSlotUnderMouse();

            if (slotUnderMouse != null) // If player didn't click on a slot, we don't care about that
            {
                EntityPlayer player = Minecraft.getMinecraft().player;
                Item itemOnCursor = player.inventory.getItemStack().getItem(); // Get stack held by the mouse
                ItemStack stackUnderMouse = slotUnderMouse.getStack();

                shouldCancel = !canItemBeMoved(itemOnCursor, slotUnderMouse, player.openContainer);

                // When shift-clicking, can't modify where baskets end up without major trouble,
                // so just disallow all shift-clicking on baskets if baskets are restricted in any way

                // Clicking on basket in player inventory
                // Baskets aren't allowed somewhere
                // Shift key is down
                if (stackUnderMouse.getItem() instanceof ItemBlockBasket
                        && GuiContainer.isShiftKeyDown()
                        && (BasketCaseConfig.basketsRestrictedToHotBar
                        || !BasketCaseConfig.basketsAllowedInContainers
                        || gui instanceof GuiShulkerBox))
                {
                    shouldCancel = true;
                }

                // Shift clicking on shulker box while basket inventory is open
                if (Block.getBlockFromItem(stackUnderMouse.getItem()) instanceof BlockShulkerBox
                        && GuiContainer.isShiftKeyDown()
                        && gui instanceof GuiBasket)
                {
                    shouldCancel = true;
                }

                // Clicking on the basket whose inventory we're currently viewing
                // This is not allowed - leads to duping glitches
                // Like moving held basket out of main hand slot and then moving stuff out of basket inventory (which is still open)
                // Or like clicking it and dropping it out of your inventory completely, then moving stuff out of basket inventory (which is still open)
                if (gui instanceof GuiBasket
                        && stackUnderMouse.getItem() instanceof ItemBlockBasket
                        && PrimalUtil.isItemStackInMainHand(player, stackUnderMouse))
                {
                    shouldCancel = true;
                }
            }
        }

        event.setCanceled(shouldCancel);
    }

    // Prevent pressing numbers to move basket in/out of hotbar, and pressing Q to eject while inventory is open
    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void guiKey(GuiScreenEvent.KeyboardInputEvent.Pre event)
    {
        GuiScreen gui = event.getGui();
        boolean shouldCancel = false;

        if (gui instanceof GuiContainer)
        {
            Slot slotUnderMouse = ((GuiContainer) gui).getSlotUnderMouse();

            if (slotUnderMouse != null)
            {
                Minecraft minecraft = Minecraft.getMinecraft();
                EntityPlayer player = Minecraft.getMinecraft().player;
                ItemStack stackUnderMouse = slotUnderMouse.getStack();

                // Prevent player from swapping an item in regular inventory with a basket in the hotbar by hovering over item and pressing hotbar slot number
                // Iterate through each of the hotbar keybinds and see if it was the one pressed
                for (int i = 0; i < minecraft.gameSettings.keyBindsHotbar.length; ++i)
                {
                    boolean keyPressedIsHotbarKey = minecraft.gameSettings.keyBindsHotbar[i].isActiveAndMatches(Keyboard.getEventKey());

                    if (keyPressedIsHotbarKey)
                    {
                        ItemStack stackInTargetHotbarSlot = player.inventory.mainInventory.get(i);

                        shouldCancel = !canItemBeMoved(stackInTargetHotbarSlot.getItem(), slotUnderMouse, player.openContainer);

                        if (stackInTargetHotbarSlot.getItem() instanceof ItemBlockBasket)
                        {
                            // If target basket is in player's main hand & basket inv open - cancel
                            if (PrimalUtil.isItemStackInMainHand(player, stackInTargetHotbarSlot))
                            {
                                shouldCancel = true;
                            }
                        }
                    }
                }

                // Prevent player from hovering over basket whose inventory we're currently viewing and pressing Q to eject it
                // When viewing a basket's inventory, the player must be holding it in their main hand
                if (minecraft.gameSettings.keyBindDrop.isActiveAndMatches(Keyboard.getEventKey())
                        && gui instanceof GuiBasket
                        && stackUnderMouse.getItem() instanceof ItemBlockBasket
                        && PrimalUtil.isItemStackInMainHand(player, stackUnderMouse))
                {
                    shouldCancel = true;
                }
            }
        }

        event.setCanceled(shouldCancel);
    }

    @SuppressWarnings({"RedundantIfStatement", "BooleanMethodIsAlwaysInverted"})
    private boolean canItemBeMoved(Item itemToBeMoved, Slot destinationSlot, Container openContainer)
    {
        IInventory destinationInventory = destinationSlot.inventory;
        int slotIndex = destinationSlot.getSlotIndex();

        if (Block.getBlockFromItem(itemToBeMoved) instanceof BlockShulkerBox)
        {
            // Basket container is open and mouse is over basket inventory (InventoryBasic is basket slots)
            if (openContainer instanceof ContainerBasket && destinationInventory instanceof InventoryBasic)
            {
                return false;
            }
        }
        // Using hotbar key to move a basket that's currently in the hotbar
        else if (itemToBeMoved instanceof ItemBlockBasket)
        {
            // Baskets can go in the crafting grid, hotbar, and offhand slot - no need to cancel
            if (destinationInventory instanceof InventoryCrafting || PrimalUtil.isSlotHotbarOrOffhand(slotIndex, destinationInventory))
            {
                return true;
            }

            // Baskets can never go in a shulker box inventory - cancel
            // ContainerLocalMenu is the shulker box inventory
            if (openContainer instanceof ContainerShulkerBox && destinationInventory instanceof ContainerLocalMenu)
            {
                return false;
            }

            // Config set so baskets can't go in player's regular inventory - cancel
            if (BasketCaseConfig.basketsRestrictedToHotBar)
            {
                // This handles trying to put basket in regular inventory
                // Trying to put basket in hotbar or offhand is handled above
                if (destinationInventory instanceof InventoryPlayer)
                {
                    return false;
                }
            }

            // Config set to baskets can't go in container inventories - cancel
            if (!BasketCaseConfig.basketsAllowedInContainers)
            {
                // Using hotbar key to move basket anywhere except player inventory (crafting grid and shulker box already caught above)
                if (!(destinationInventory instanceof InventoryPlayer))
                {
                    return false;
                }
            }
        }

        return true;
    }

    /*
     * SHOW BASKET CONTENTS ON HOVER (in tool tip)
     */

    // This gets and saves the location of the mouse for use in the later PostText event (which doesn't have access to the mouse position)
    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public void onPreToolTipRender(RenderTooltipEvent.Pre event)
    {
        mouseX = event.getX();
    }

    // Has to be done in PostText to prevent other stuff overlapping the tool tip
    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public void onToolTipRender(RenderTooltipEvent.PostText event)
    {
        ItemStack itemStack = event.getStack();

        if (Block.getBlockFromItem(itemStack.getItem()) instanceof BlockTileEntityBasket)
        {
            if (isShiftKeyDown())
            {
                ItemStackHandlerBasket basketInventory = (ItemStackHandlerBasket) itemStack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

                if (basketInventory != null)
                {
                    Minecraft minecraft = Minecraft.getMinecraft();
                    int inventorySize = basketInventory.getSlots();
                    int rows = 2; // To match normal basket inventory layout
                    int columns = inventorySize / rows;
                    int slotWidth = 17;
                    int screenCenter = new ScaledResolution(minecraft).getScaledWidth() / 2;
                    // I think toolTipX and Y are the top left corner of the tool tip text, not the background box
                    // That's why leftEdge and rightEdge have to be adjusted, to pull the background box up and to the left of the text
                    // X and Y origins are in top left corner of screen (to raise an object, lower the Y)
                    int toolTipX = event.getX();
                    int toolTipY = event.getY();
                    int zLevel = 300;
                    int toolTipW = event.getWidth();
                    int inventoryWidth = columns * slotWidth;
                    // Outer boundaries of inventory tool tip box
                    int leftEdge = this.mouseX > screenCenter ? toolTipX - 3 : toolTipX + toolTipW - inventoryWidth + 2;
                    int rightEdge = leftEdge + inventoryWidth + 1;
                    int bottomEdge = toolTipY - 6; // -6 gives 1 pixel between regular tool tip and the inventory tool tip
                    int topEdge = bottomEdge - (rows * slotWidth) - 1;
                    // Colors - copied from Forge's tool tip class
                    int backgroundColor = 0xF0100010;
                    int borderColorStart = 0x505000FF;
                    int borderColorEnd = (borderColorStart & 0xFEFEFE) >> 1 | borderColorStart & 0xFF000000;

                    // Draw inventory background

                    // Background body
                    GuiUtils.drawGradientRect(zLevel, leftEdge, topEdge, rightEdge, bottomEdge, backgroundColor, backgroundColor);
                    // Black border - left
                    GuiUtils.drawGradientRect(zLevel, leftEdge - 1, topEdge, leftEdge, bottomEdge, backgroundColor, backgroundColor);
                    // Black border - right
                    GuiUtils.drawGradientRect(zLevel, rightEdge, topEdge, rightEdge + 1, bottomEdge, backgroundColor, backgroundColor);
                    // Black border - top
                    GuiUtils.drawGradientRect(zLevel, leftEdge, topEdge - 1, rightEdge, topEdge, backgroundColor, backgroundColor);
                    // Black border - bottom
                    GuiUtils.drawGradientRect(zLevel, leftEdge, bottomEdge, rightEdge, bottomEdge + 1, backgroundColor, backgroundColor);

                    // Purple lines - vertical
                    for (int i = 0; i <= columns; i++)
                    {
                        int spacer = i * slotWidth;

                        GuiUtils.drawGradientRect(zLevel, leftEdge + spacer, topEdge, leftEdge + spacer + 1, bottomEdge, borderColorStart, borderColorEnd);
                    }

                    // Purple lines - horizontal
                    for (int i = 0; i <= rows; i++)
                    {
                        int spacer = i * slotWidth;

                        GuiUtils.drawGradientRect(zLevel, leftEdge, topEdge + spacer, rightEdge, topEdge + spacer + 1, borderColorStart, borderColorEnd);
                    }

                    // Draw inventory items

                    RenderHelper.enableGUIStandardItemLighting();
                    RenderItem renderItem = minecraft.getRenderItem();

                    for (int i = 0; i < inventorySize; i++)
                    {
                        ItemStack itemInBasket = basketInventory.getStackInSlot(i);
                        int currentX = leftEdge + ((i % columns) * slotWidth) + 1;
                        int currentY = topEdge + ((i / columns) * slotWidth) + 1;

                        if (!itemInBasket.isEmpty())
                        {
                            // zLevel must be this high to prevent other stuff overlapping the tool tip
                            renderItem.zLevel = 300;
                            renderItem.renderItemAndEffectIntoGUI(itemInBasket, currentX, currentY);
                            renderItem.renderItemOverlayIntoGUI(event.getFontRenderer(), itemInBasket, currentX, currentY, null); // # items in stack, durability bar
                        }
                    }

                    RenderHelper.disableStandardItemLighting();
                }
            }
        }
    }

    // Sync changes to basket inventory from server to client while basket inventory is still open, so basket tool tip can show accurate contents
    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event)
    {
        // We only need EntityPlayerMP because the server has the correct inventory to send to the client
        if (event.phase == TickEvent.Phase.START && event.player instanceof EntityPlayerMP)
        {
            EntityPlayerMP player = (EntityPlayerMP) event.player;

            // Just a safeguard - should never be null since it gets set to inventoryContainer as soon as the player closes whatever inventory they had open
            // Because of this, the server always thinks the player's inventory is open
            // We only need to sync when a basket container is open, since that's the only time changes can be made to its inventory
            if (player.openContainer != null && player.openContainer instanceof ContainerBasket)
            {
                Container container = player.openContainer;

                // Iterate over the entire player inventory to find baskets
                for (int i = 0; i < container.inventorySlots.size(); i++)
                {
                    ItemStack slotStack = container.inventorySlots.get(i).getStack();

                    if (!slotStack.isEmpty() && slotStack.getItem() instanceof ItemBlockBasket)
                    {
                        ItemStackHandlerBasket slotStackInv = (ItemStackHandlerBasket) slotStack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

                        if (slotStackInv != null)
                        {
                            // Save the basket's inventory to NBT and send to the client
                            NBTTagCompound nbt = slotStackInv.serializeNBT();
                            BasketInventoryUpdate message = new BasketInventoryUpdate(i, nbt);

                            PacketHandler.network.sendTo(message, player);
                        }
                    }
                }
            }
        }
    }

    /*
     *  REMAP BLOCKS AND ITEMS
     */

    @SubscribeEvent
    public void missingMappingBlockEvent(RegistryEvent.MissingMappings<Block> event)
    {
        ImmutableList<Mapping<Block>> mappings = event.getMappings();

        // Can't get anything out of an ImmutableList without knowing the index
        // So we have to iterate through it to check for block mappings we're interested in
        for (Mapping<Block> mapping : mappings)
        {
            String missingName = mapping.key.toString();

            switch (missingName)
            {
                case "basketcase:container_basket_small":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapBlockTo(mapping, "basketcase:basket_small_bark_oak", BlockRegistry.getBlocks());
                    else
                        PrimalUtilReg.remapBlockTo(mapping, "basketcase:basket_small_generic", BlockRegistry.getBlocks());
                    break;
                case "basketcase:container_basket_medium":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapBlockTo(mapping, "basketcase:basket_medium_bark_oak", BlockRegistry.getBlocks());
                    else
                        PrimalUtilReg.remapBlockTo(mapping, "basketcase:basket_medium_generic", BlockRegistry.getBlocks());
                    break;
                case "basketcase:container_basket_large":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapBlockTo(mapping, "basketcase:basket_large_bark_oak", BlockRegistry.getBlocks());
                    else
                        PrimalUtilReg.remapBlockTo(mapping, "basketcase:basket_large_generic", BlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_oak":
                    PrimalUtilReg.remapBlockTo(mapping, "primallib:log_stripped_oak", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_spruce":
                    PrimalUtilReg.remapBlockTo(mapping, "primallib:log_stripped_spruce", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_birch":
                    PrimalUtilReg.remapBlockTo(mapping, "primallib:log_stripped_birch", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_jungle":
                    PrimalUtilReg.remapBlockTo(mapping, "primallib:log_stripped_jungle", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_acacia":
                    PrimalUtilReg.remapBlockTo(mapping, "primallib:log_stripped_acacia", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_dark_oak":
                    PrimalUtilReg.remapBlockTo(mapping, "primallib:log_stripped_dark_oak", PrimalBlockRegistry.getBlocks());
                    break;
            }
        }
    }

    @SubscribeEvent
    public void missingMappingItemEvent(RegistryEvent.MissingMappings<Item> event)
    {
        ImmutableList<Mapping<Item>> mappings = event.getMappings();

        // Can't get anything out of an ImmutableList without knowing the index
        // So we have to iterate through it to check for block mappings we're interested in
        for (Mapping<Item> mapping : mappings)
        {
            String missingName = mapping.key.toString();

            switch (missingName)
            {
                case "basketcase:container_basket_small":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemBlockTo(mapping, "basketcase:basket_small_bark_oak", BlockRegistry.getBlocks());
                    else
                        PrimalUtilReg.remapItemBlockTo(mapping, "basketcase:basket_small_generic", BlockRegistry.getBlocks());
                    break;
                case "basketcase:container_basket_medium":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemBlockTo(mapping, "basketcase:basket_medium_bark_oak", BlockRegistry.getBlocks());
                    else
                        PrimalUtilReg.remapItemBlockTo(mapping, "basketcase:basket_medium_generic", BlockRegistry.getBlocks());
                    break;
                case "basketcase:container_basket_large":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemBlockTo(mapping, "basketcase:basket_large_bark_oak", BlockRegistry.getBlocks());
                    else
                        PrimalUtilReg.remapItemBlockTo(mapping, "basketcase:basket_large_generic", BlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_oak":
                    PrimalUtilReg.remapItemBlockTo(mapping, "primallib:log_stripped_oak", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_spruce":
                    PrimalUtilReg.remapItemBlockTo(mapping, "primallib:log_stripped_spruce", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_birch":
                    PrimalUtilReg.remapItemBlockTo(mapping, "primallib:log_stripped_birch", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_jungle":
                    PrimalUtilReg.remapItemBlockTo(mapping, "primallib:log_stripped_jungle", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_acacia":
                    PrimalUtilReg.remapItemBlockTo(mapping, "primallib:log_stripped_acacia", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:log_stripped_dark_oak":
                    PrimalUtilReg.remapItemBlockTo(mapping, "primallib:log_stripped_dark_oak", PrimalBlockRegistry.getBlocks());
                    break;
                case "basketcase:item_wicker_small":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:wicker_small_bark_oak", ItemRegistry.getItems());
                    else
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:wicker_small_generic", ItemRegistry.getItems());
                    break;
                case "basketcase:item_wicker_medium":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:wicker_medium_bark_oak", ItemRegistry.getItems());
                    else
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:wicker_medium_generic", ItemRegistry.getItems());
                    break;
                case "basketcase:item_wicker_large":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:wicker_large_bark_oak", ItemRegistry.getItems());
                    else
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:wicker_large_generic", ItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_strips":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:bark_strips_oak", ItemRegistry.getItems());
                    else
                        PrimalUtilReg.remapItemTo(mapping, "primallib:bark_strips_generic", ItemRegistry.getItems());
                    break;
                case "basketcase:item_twig":
                    if (BasketCaseConfig.allowWoodSpecificBaskets)
                        PrimalUtilReg.remapItemTo(mapping, "basketcase:twig_oak", ItemRegistry.getItems());
                    else
                        PrimalUtilReg.remapItemTo(mapping, "primallib:twig_generic", ItemRegistry.getItems());
                    break;
                case "basketcase:item_plant_fiber":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:plant_fiber", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_cordage":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:twine", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_oak":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:bark_oak", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_spruce":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:bark_spruce", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_birch":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:bark_birch", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_jungle":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:bark_jungle", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_acacia":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:bark_acacia", PrimalItemRegistry.getItems());
                    break;
                case "basketcase:item_bark_dark_oak":
                    PrimalUtilReg.remapItemTo(mapping, "primallib:bark_dark_oak", PrimalItemRegistry.getItems());
                    break;
            }
        }
    }
}
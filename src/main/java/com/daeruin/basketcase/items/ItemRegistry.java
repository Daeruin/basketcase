package com.daeruin.basketcase.items;

import com.daeruin.primallib.items.ItemBase;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.LinkedHashSet;

public final class ItemRegistry
{
    // BARK STRIPS - generic bark strips are in PrimalLib
    public static final Item BARK_STRIPS_ACACIA = new ItemBase("bark_strips_acacia", 30);
    public static final Item BARK_STRIPS_BIRCH = new ItemBase("bark_strips_birch", 30);
    public static final Item BARK_STRIPS_DARK_OAK = new ItemBase("bark_strips_dark_oak", 30);
    public static final Item BARK_STRIPS_JUNGLE = new ItemBase("bark_strips_jungle", 30);
    public static final Item BARK_STRIPS_OAK = new ItemBase("bark_strips_oak", 30);
    public static final Item BARK_STRIPS_SPRUCE = new ItemBase("bark_strips_spruce", 30);
    // TWIGS - generic twigs are in PrimalLib
    public static final Item TWIG_ACACIA = new ItemBase("twig_acacia", 30);
    public static final Item TWIG_BIRCH = new ItemBase("twig_birch", 30);
    public static final Item TWIG_DARK_OAK = new ItemBase("twig_dark_oak", 30);
    public static final Item TWIG_JUNGLE = new ItemBase("twig_jungle", 30);
    public static final Item TWIG_OAK = new ItemBase("twig_oak", 30);
    public static final Item TWIG_SPRUCE = new ItemBase("twig_spruce", 30);
    // SMALL WICKER
    public static final Item WICKER_SMALL_GENERIC = new ItemBase("wicker_small_generic", 80);
    public static final Item WICKER_SMALL_BARK_ACACIA = new ItemBase("wicker_small_bark_acacia", 80);
    public static final Item WICKER_SMALL_BARK_BIRCH = new ItemBase("wicker_small_bark_birch", 80);
    public static final Item WICKER_SMALL_BARK_DARK_OAK = new ItemBase("wicker_small_bark_dark_oak", 80);
    public static final Item WICKER_SMALL_BARK_JUNGLE = new ItemBase("wicker_small_bark_jungle", 80);
    public static final Item WICKER_SMALL_BARK_OAK = new ItemBase("wicker_small_bark_oak", 80);
    public static final Item WICKER_SMALL_BARK_SPRUCE = new ItemBase("wicker_small_bark_spruce", 80);
    public static final Item WICKER_SMALL_TWIG_ACACIA = new ItemBase("wicker_small_twig_acacia", 80);
    public static final Item WICKER_SMALL_TWIG_BIRCH = new ItemBase("wicker_small_twig_birch", 80);
    public static final Item WICKER_SMALL_TWIG_DARK_OAK = new ItemBase("wicker_small_twig_dark_oak", 80);
    public static final Item WICKER_SMALL_TWIG_JUNGLE = new ItemBase("wicker_small_twig_jungle", 80);
    public static final Item WICKER_SMALL_TWIG_OAK = new ItemBase("wicker_small_twig_oak", 80);
    public static final Item WICKER_SMALL_TWIG_SPRUCE = new ItemBase("wicker_small_twig_spruce", 80);
    // MEDIUM WICKER
    public static final Item WICKER_MEDIUM_GENERIC = new ItemBase("wicker_medium_generic", 100);
    public static final Item WICKER_MEDIUM_BARK_ACACIA = new ItemBase("wicker_medium_bark_acacia", 100);
    public static final Item WICKER_MEDIUM_BARK_BIRCH = new ItemBase("wicker_medium_bark_birch", 100);
    public static final Item WICKER_MEDIUM_BARK_DARK_OAK = new ItemBase("wicker_medium_bark_dark_oak", 100);
    public static final Item WICKER_MEDIUM_BARK_JUNGLE = new ItemBase("wicker_medium_bark_jungle", 100);
    public static final Item WICKER_MEDIUM_BARK_OAK = new ItemBase("wicker_medium_bark_oak", 100);
    public static final Item WICKER_MEDIUM_BARK_SPRUCE = new ItemBase("wicker_medium_bark_spruce", 100);
    public static final Item WICKER_MEDIUM_TWIG_ACACIA = new ItemBase("wicker_medium_twig_acacia", 100);
    public static final Item WICKER_MEDIUM_TWIG_BIRCH = new ItemBase("wicker_medium_twig_birch", 100);
    public static final Item WICKER_MEDIUM_TWIG_DARK_OAK = new ItemBase("wicker_medium_twig_dark_oak", 100);
    public static final Item WICKER_MEDIUM_TWIG_JUNGLE = new ItemBase("wicker_medium_twig_jungle", 100);
    public static final Item WICKER_MEDIUM_TWIG_OAK = new ItemBase("wicker_medium_twig_oak", 100);
    public static final Item WICKER_MEDIUM_TWIG_SPRUCE = new ItemBase("wicker_medium_twig_spruce", 100);
    // LARGE WICKER
    public static final Item WICKER_LARGE_GENERIC = new ItemBase("wicker_large_generic", 120);
    public static final Item WICKER_LARGE_BARK_ACACIA = new ItemBase("wicker_large_bark_acacia", 120);
    public static final Item WICKER_LARGE_BARK_BIRCH = new ItemBase("wicker_large_bark_birch", 120);
    public static final Item WICKER_LARGE_BARK_DARK_OAK = new ItemBase("wicker_large_bark_dark_oak", 120);
    public static final Item WICKER_LARGE_BARK_JUNGLE = new ItemBase("wicker_large_bark_jungle", 120);
    public static final Item WICKER_LARGE_BARK_OAK = new ItemBase("wicker_large_bark_oak", 120);
    public static final Item WICKER_LARGE_BARK_SPRUCE = new ItemBase("wicker_large_bark_spruce", 120);
    public static final Item WICKER_LARGE_TWIG_ACACIA = new ItemBase("wicker_large_twig_acacia", 120);
    public static final Item WICKER_LARGE_TWIG_BIRCH = new ItemBase("wicker_large_twig_birch", 120);
    public static final Item WICKER_LARGE_TWIG_DARK_OAK = new ItemBase("wicker_large_twig_dark_oak", 120);
    public static final Item WICKER_LARGE_TWIG_JUNGLE = new ItemBase("wicker_large_twig_jungle", 120);
    public static final Item WICKER_LARGE_TWIG_OAK = new ItemBase("wicker_large_twig_oak", 120);
    public static final Item WICKER_LARGE_TWIG_SPRUCE = new ItemBase("wicker_large_twig_spruce", 120);

    // Adding new items to this HashSet will automatically register the item's model
    public static LinkedHashSet<Item> getItems()
    {
        // Using a LinkedHashSet to maintain the desired order in the creative tab
        LinkedHashSet<Item> ITEMS = new LinkedHashSet<>();

        ITEMS.add(TWIG_ACACIA);
        ITEMS.add(BARK_STRIPS_ACACIA);
        ITEMS.add(WICKER_SMALL_BARK_ACACIA);
        ITEMS.add(WICKER_MEDIUM_BARK_ACACIA);
        ITEMS.add(WICKER_LARGE_BARK_ACACIA);
        ITEMS.add(WICKER_SMALL_TWIG_ACACIA);
        ITEMS.add(WICKER_MEDIUM_TWIG_ACACIA);
        ITEMS.add(WICKER_LARGE_TWIG_ACACIA);
        ITEMS.add(TWIG_BIRCH);
        ITEMS.add(BARK_STRIPS_BIRCH);
        ITEMS.add(WICKER_SMALL_BARK_BIRCH);
        ITEMS.add(WICKER_MEDIUM_BARK_BIRCH);
        ITEMS.add(WICKER_LARGE_BARK_BIRCH);
        ITEMS.add(WICKER_SMALL_TWIG_BIRCH);
        ITEMS.add(WICKER_MEDIUM_TWIG_BIRCH);
        ITEMS.add(WICKER_LARGE_TWIG_BIRCH);
        ITEMS.add(TWIG_DARK_OAK);
        ITEMS.add(BARK_STRIPS_DARK_OAK);
        ITEMS.add(WICKER_SMALL_BARK_DARK_OAK);
        ITEMS.add(WICKER_MEDIUM_BARK_DARK_OAK);
        ITEMS.add(WICKER_LARGE_BARK_DARK_OAK);
        ITEMS.add(WICKER_SMALL_TWIG_DARK_OAK);
        ITEMS.add(WICKER_MEDIUM_TWIG_DARK_OAK);
        ITEMS.add(WICKER_LARGE_TWIG_DARK_OAK);
        ITEMS.add(TWIG_JUNGLE);
        ITEMS.add(BARK_STRIPS_JUNGLE);
        ITEMS.add(WICKER_SMALL_BARK_JUNGLE);
        ITEMS.add(WICKER_MEDIUM_BARK_JUNGLE);
        ITEMS.add(WICKER_LARGE_BARK_JUNGLE);
        ITEMS.add(WICKER_SMALL_TWIG_JUNGLE);
        ITEMS.add(WICKER_MEDIUM_TWIG_JUNGLE);
        ITEMS.add(WICKER_LARGE_TWIG_JUNGLE);
        ITEMS.add(TWIG_OAK);
        ITEMS.add(BARK_STRIPS_OAK);
        ITEMS.add(WICKER_SMALL_BARK_OAK);
        ITEMS.add(WICKER_MEDIUM_BARK_OAK);
        ITEMS.add(WICKER_LARGE_BARK_OAK);
        ITEMS.add(WICKER_SMALL_TWIG_OAK);
        ITEMS.add(WICKER_MEDIUM_TWIG_OAK);
        ITEMS.add(WICKER_LARGE_TWIG_OAK);
        ITEMS.add(TWIG_SPRUCE);
        ITEMS.add(BARK_STRIPS_SPRUCE);
        ITEMS.add(WICKER_SMALL_BARK_SPRUCE);
        ITEMS.add(WICKER_MEDIUM_BARK_SPRUCE);
        ITEMS.add(WICKER_LARGE_BARK_SPRUCE);
        ITEMS.add(WICKER_SMALL_TWIG_SPRUCE);
        ITEMS.add(WICKER_MEDIUM_TWIG_SPRUCE);
        ITEMS.add(WICKER_LARGE_TWIG_SPRUCE);
        ITEMS.add(WICKER_SMALL_GENERIC);
        ITEMS.add(WICKER_MEDIUM_GENERIC);
        ITEMS.add(WICKER_LARGE_GENERIC);

        return ITEMS;
    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event)
        {
            PrimalUtilReg.registerItems(event, getItems());
        }
    }
}

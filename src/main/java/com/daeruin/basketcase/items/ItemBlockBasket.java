package com.daeruin.basketcase.items;

import com.daeruin.basketcase.BasketCase;
import com.daeruin.basketcase.blocks.BlockTileEntityBasket;
import com.daeruin.basketcase.config.BasketCaseConfig;
import com.daeruin.basketcase.inventory.BasketInventoryProvider;
import net.minecraft.block.Block;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

public class ItemBlockBasket extends ItemBlock
{
    public ItemBlockBasket(Block block)
    {
        super(block);
        this.setMaxStackSize(1);
    }

    // When the ItemStack is created, attach a new inventory to it
    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, @Nullable NBTTagCompound compound)
    {
        BlockTileEntityBasket block = (BlockTileEntityBasket) this.getBlock();

        return new BasketInventoryProvider(block.getNumSlots(), block.getMaxStackSize(), block.getGuiTexture());
    }

    // Baskets can only be placed on solid surfaces
    @Override
    @Nonnull
    @ParametersAreNonnullByDefault
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        IBlockState state = worldIn.getBlockState(pos);
        Block block = state.getBlock();
        BlockPos posToCheck = pos;

        if (!block.isReplaceable(worldIn, pos))
        {
            posToCheck = pos.offset(facing).down();
            state = worldIn.getBlockState(posToCheck);
        }

        // If block below has a solid top, allow placement per super class
        if (state.getBlockFaceShape(worldIn, posToCheck, EnumFacing.UP) == BlockFaceShape.SOLID)
        {
            return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
        }
        else // Block below doesn't have a solid top; don't allow basket to be placed on it
        {
            return EnumActionResult.FAIL;
        }
    }

    @Override
    @Nonnull
    @ParametersAreNonnullByDefault
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand)
    {
        if (!BasketCaseConfig.basketsAreDecorativeOnly)
        {
            // All information about the GUI is stored in the tile entity or the held item's NBT data
            // The GuiHandler already has access to both
            player.openGui(BasketCase.INSTANCE, 0, world, 0, 0, 0);
        }

        return super.onItemRightClick(world, player, hand);
    }

    @Override
    @ParametersAreNonnullByDefault
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn)
    {
        if (!BasketCaseConfig.basketsAreDecorativeOnly)
        {
            tooltip.add(I18n.format("tooltip.basket"));
        }

        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}

package com.daeruin.basketcase.recipes;

import com.daeruin.basketcase.items.ItemRegistry;
import com.daeruin.primallib.util.PrimalUtilRecipe;

public class OreDictRegistry
{
    public static void registerOreDictEntries()
    {
        PrimalUtilRecipe.addAlternateIngredients("cordageStrong", ItemRegistry.BARK_STRIPS_ACACIA, ItemRegistry.BARK_STRIPS_BIRCH, ItemRegistry.BARK_STRIPS_DARK_OAK, ItemRegistry.BARK_STRIPS_JUNGLE, ItemRegistry.BARK_STRIPS_OAK, ItemRegistry.BARK_STRIPS_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("cordage", ItemRegistry.BARK_STRIPS_ACACIA, ItemRegistry.BARK_STRIPS_BIRCH, ItemRegistry.BARK_STRIPS_DARK_OAK, ItemRegistry.BARK_STRIPS_JUNGLE, ItemRegistry.BARK_STRIPS_OAK, ItemRegistry.BARK_STRIPS_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("twig", ItemRegistry.TWIG_ACACIA, ItemRegistry.TWIG_BIRCH, ItemRegistry.TWIG_DARK_OAK, ItemRegistry.TWIG_JUNGLE, ItemRegistry.TWIG_OAK, ItemRegistry.TWIG_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("barkStrips", ItemRegistry.BARK_STRIPS_ACACIA, ItemRegistry.BARK_STRIPS_BIRCH, ItemRegistry.BARK_STRIPS_DARK_OAK, ItemRegistry.BARK_STRIPS_JUNGLE, ItemRegistry.BARK_STRIPS_OAK, ItemRegistry.BARK_STRIPS_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("wicker",
                ItemRegistry.WICKER_SMALL_GENERIC,
                ItemRegistry.WICKER_SMALL_BARK_ACACIA,
                ItemRegistry.WICKER_SMALL_BARK_BIRCH,
                ItemRegistry.WICKER_SMALL_BARK_DARK_OAK,
                ItemRegistry.WICKER_SMALL_BARK_JUNGLE,
                ItemRegistry.WICKER_SMALL_BARK_OAK,
                ItemRegistry.WICKER_SMALL_BARK_SPRUCE,
                ItemRegistry.WICKER_SMALL_TWIG_ACACIA,
                ItemRegistry.WICKER_SMALL_TWIG_BIRCH,
                ItemRegistry.WICKER_SMALL_TWIG_DARK_OAK,
                ItemRegistry.WICKER_SMALL_TWIG_JUNGLE,
                ItemRegistry.WICKER_SMALL_TWIG_OAK,
                ItemRegistry.WICKER_SMALL_TWIG_SPRUCE,
                ItemRegistry.WICKER_MEDIUM_GENERIC,
                ItemRegistry.WICKER_MEDIUM_BARK_ACACIA,
                ItemRegistry.WICKER_MEDIUM_BARK_BIRCH,
                ItemRegistry.WICKER_MEDIUM_BARK_DARK_OAK,
                ItemRegistry.WICKER_MEDIUM_BARK_JUNGLE,
                ItemRegistry.WICKER_MEDIUM_BARK_OAK,
                ItemRegistry.WICKER_MEDIUM_BARK_SPRUCE,
                ItemRegistry.WICKER_MEDIUM_TWIG_ACACIA,
                ItemRegistry.WICKER_MEDIUM_TWIG_BIRCH,
                ItemRegistry.WICKER_MEDIUM_TWIG_DARK_OAK,
                ItemRegistry.WICKER_MEDIUM_TWIG_JUNGLE,
                ItemRegistry.WICKER_MEDIUM_TWIG_OAK,
                ItemRegistry.WICKER_MEDIUM_TWIG_SPRUCE,
                ItemRegistry.WICKER_LARGE_GENERIC,
                ItemRegistry.WICKER_LARGE_BARK_ACACIA,
                ItemRegistry.WICKER_LARGE_BARK_BIRCH,
                ItemRegistry.WICKER_LARGE_BARK_DARK_OAK,
                ItemRegistry.WICKER_LARGE_BARK_JUNGLE,
                ItemRegistry.WICKER_LARGE_BARK_OAK,
                ItemRegistry.WICKER_LARGE_BARK_SPRUCE,
                ItemRegistry.WICKER_LARGE_TWIG_ACACIA,
                ItemRegistry.WICKER_LARGE_TWIG_BIRCH,
                ItemRegistry.WICKER_LARGE_TWIG_DARK_OAK,
                ItemRegistry.WICKER_LARGE_TWIG_JUNGLE,
                ItemRegistry.WICKER_LARGE_TWIG_OAK,
                ItemRegistry.WICKER_LARGE_TWIG_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("wickerSmall",
                ItemRegistry.WICKER_SMALL_GENERIC,
                ItemRegistry.WICKER_SMALL_BARK_ACACIA,
                ItemRegistry.WICKER_SMALL_BARK_BIRCH,
                ItemRegistry.WICKER_SMALL_BARK_DARK_OAK,
                ItemRegistry.WICKER_SMALL_BARK_JUNGLE,
                ItemRegistry.WICKER_SMALL_BARK_OAK,
                ItemRegistry.WICKER_SMALL_BARK_SPRUCE,
                ItemRegistry.WICKER_SMALL_TWIG_ACACIA,
                ItemRegistry.WICKER_SMALL_TWIG_BIRCH,
                ItemRegistry.WICKER_SMALL_TWIG_DARK_OAK,
                ItemRegistry.WICKER_SMALL_TWIG_JUNGLE,
                ItemRegistry.WICKER_SMALL_TWIG_OAK,
                ItemRegistry.WICKER_SMALL_TWIG_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("wickerMedium",
                ItemRegistry.WICKER_MEDIUM_GENERIC,
                ItemRegistry.WICKER_MEDIUM_BARK_ACACIA,
                ItemRegistry.WICKER_MEDIUM_BARK_BIRCH,
                ItemRegistry.WICKER_MEDIUM_BARK_DARK_OAK,
                ItemRegistry.WICKER_MEDIUM_BARK_JUNGLE,
                ItemRegistry.WICKER_MEDIUM_BARK_OAK,
                ItemRegistry.WICKER_MEDIUM_BARK_SPRUCE,
                ItemRegistry.WICKER_MEDIUM_TWIG_ACACIA,
                ItemRegistry.WICKER_MEDIUM_TWIG_BIRCH,
                ItemRegistry.WICKER_MEDIUM_TWIG_DARK_OAK,
                ItemRegistry.WICKER_MEDIUM_TWIG_JUNGLE,
                ItemRegistry.WICKER_MEDIUM_TWIG_OAK,
                ItemRegistry.WICKER_MEDIUM_TWIG_SPRUCE);
        PrimalUtilRecipe.addAlternateIngredients("wickerLarge",
                ItemRegistry.WICKER_LARGE_GENERIC,
                ItemRegistry.WICKER_LARGE_BARK_ACACIA,
                ItemRegistry.WICKER_LARGE_BARK_BIRCH,
                ItemRegistry.WICKER_LARGE_BARK_DARK_OAK,
                ItemRegistry.WICKER_LARGE_BARK_JUNGLE,
                ItemRegistry.WICKER_LARGE_BARK_OAK,
                ItemRegistry.WICKER_LARGE_BARK_SPRUCE,
                ItemRegistry.WICKER_LARGE_TWIG_ACACIA,
                ItemRegistry.WICKER_LARGE_TWIG_BIRCH,
                ItemRegistry.WICKER_LARGE_TWIG_DARK_OAK,
                ItemRegistry.WICKER_LARGE_TWIG_JUNGLE,
                ItemRegistry.WICKER_LARGE_TWIG_OAK,
                ItemRegistry.WICKER_LARGE_TWIG_SPRUCE);
    }
}

package com.daeruin.basketcase.recipes;

import com.daeruin.basketcase.config.BasketCaseConfig;
import com.daeruin.primallib.config.PrimalConfig;
import com.google.gson.JsonObject;
import net.minecraft.util.JsonUtils;
import net.minecraftforge.common.crafting.IConditionFactory;
import net.minecraftforge.common.crafting.JsonContext;

import java.util.function.BooleanSupplier;

@SuppressWarnings("unused")
public class RecipeConditionFactory implements IConditionFactory
{
    @Override
    public BooleanSupplier parse(JsonContext context, JsonObject json)
    {
        boolean allowCrafting;

        // Get the recipe name from the JSON that's being passed in
        // Expected JSON format: {"type":"basketcase:is_recipe_allowed","recipe_name":"large_basket"}
        String recipeName = JsonUtils.getString(json, "recipe_name");

        // Figure out which recipe is being parsed, and get the config value for that recipe
        switch (recipeName)
        {
            // If recipes for wood-specific bark strips are disabled, players will also be unable to craft wood-specific baskets
            case "bark_strips":
                allowCrafting = PrimalConfig.BARK_AND_STRIPPED_LOGS.allowBarkStrips;
                break;
            case "generic_basket":
                allowCrafting = !BasketCaseConfig.allowWoodSpecificBaskets;
                break;
            case "wood_specific_basket":
                allowCrafting = BasketCaseConfig.allowWoodSpecificBaskets;
                break;
            case "small_basket":
                allowCrafting = BasketCaseConfig.allowCraftingSmallBaskets;
                break;
            case "medium_basket":
                allowCrafting = BasketCaseConfig.allowCraftingMediumBaskets;
                break;
            case "large_basket":
                allowCrafting = BasketCaseConfig.allowCraftingLargeBaskets;
                break;
            default:
                allowCrafting = true;
        }

        // This lambda stuff apparently turns a boolean into a BooleanSupplier
        return () -> allowCrafting;
    }
}

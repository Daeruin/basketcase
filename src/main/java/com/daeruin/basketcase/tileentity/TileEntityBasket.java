package com.daeruin.basketcase.tileentity;

import com.daeruin.basketcase.inventory.ItemStackHandlerBasket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileEntityBasket extends TileEntity
{
    public int numSlots;
    private int maxStackSize;
    private ResourceLocation guiTexture;
    private ItemStackHandlerBasket inventory;

    public TileEntityBasket()
    {
    }

    public void initializeTileEntity(final int numSlots, final int maxStackSize, ResourceLocation guiTexture)
    {
        this.numSlots = numSlots;
        this.maxStackSize = maxStackSize;
        this.guiTexture = guiTexture;
        this.inventory = new ItemStackHandlerBasket(this.numSlots, this.maxStackSize, this.guiTexture);
    }

    // Will close the container if it becomes unavailable, e.g. if the basket is broken
    public boolean isUsableByPlayer(EntityPlayer player)
    {
        return this.world.getTileEntity(this.getPos()) == this && player.getDistanceSq(this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5) < 64;
    }

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing)
    {
        //noinspection unchecked
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T) inventory : super.getCapability(capability, facing);
    }

    @Override
    @Nonnull
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        // Write my custom data to the tile entity's NBT
        compound.setInteger("numSlots", this.numSlots);
        compound.setInteger("maxStackSize", this.maxStackSize);
        compound.setString("guiTexture", this.guiTexture.toString());

        // Write the inventory contents to the tile entity's NBT
        compound.setTag("inventory", inventory.serializeNBT());

        // Super stores the tile entity's coordinates and ResourceLocation
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        // Get my custom data out of the tile entity's NBT
        int numSlots = compound.getInteger("numSlots");
        int maxStackSize = compound.getInteger("maxStackSize");
        ResourceLocation guiTexture = new ResourceLocation(compound.getString("guiTexture"));

        // Set the tile entity's fields using the NBT data - required for tile entity to be instantiated when loading a world
        initializeTileEntity(numSlots, maxStackSize, guiTexture);

        // Load up the inventory capability with the tile entity's ItemStacks
        inventory.deserializeNBT(compound.getCompoundTag("inventory"));

        // Set the tile entity's coordinates
        super.readFromNBT(compound);
    }

    // Sync tile entity data between client and server - nothing will work without this!
    // Needed because I'm storing more than just an inventory
    // Seems to be on server only, syncs with client when opening inventory or placing block

    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(this.pos, 3, this.getUpdateTag());
    }

    @Override
    @Nonnull
    public NBTTagCompound getUpdateTag()
    {
        return this.writeToNBT(new NBTTagCompound());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet)
    {
        super.onDataPacket(net, packet);
        handleUpdateTag(packet.getNbtCompound());
    }
}

package com.daeruin.basketcase.proxy;

import com.daeruin.primallib.proxy.IProxy;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SideOnly(Side.SERVER)
public class ServerProxy implements IProxy
{
    @Override
    public void preInit()
    {
    }

    @Override
    public void init()
    {
    }

    @Override
    public void postInit()
    {
    }

    @Nonnull
    @Override
    public IThreadListener getThreadListener(MessageContext context)
    {
        if (context.side.isServer())
        {
            return context.getServerHandler().player.server;
        }
        else
        {
            throw new RuntimeException("Tried to get the IThreadListener from a client-side MessageContext on the dedicated server");
        }
    }

    @Override
    @Nullable
    public EntityPlayer getPlayer(MessageContext context)
    {
        if (context.side.isServer())
        {
            return context.getServerHandler().player;
        }
        else
        {
            throw new RuntimeException("Tried to get the player from a client-side MessageContext on the dedicated server");
        }
    }
}

package com.daeruin.basketcase.util;

import com.daeruin.basketcase.items.ItemRegistry;
import net.minecraft.block.*;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

public class BasketCaseUtil
{
    private static BlockPlanks.EnumType getLeafType(IBlockState blockState)
    {
        Block block = blockState.getBlock();
        BlockPlanks.EnumType type = BlockPlanks.EnumType.OAK; // Default

        if (block instanceof BlockLeaves)
        {
            if (block == Blocks.LEAVES)
            {
                type = blockState.getValue(BlockOldLeaf.VARIANT);
            }
            else if (block == Blocks.LEAVES2)
            {
                type = blockState.getValue(BlockNewLeaf.VARIANT);
            }
        }

        return type;
    }

    // This method is called when wood-specific baskets are enabled
    // Determines what twig should drop given a certain leaf block
    // Any block other than vanilla leaves will default to oak twigs
    public static Item getTwig(IBlockState blockState)
    {
        Item itemToDrop = ItemRegistry.TWIG_OAK; // Default

        if (blockState.getBlock() instanceof BlockLeaves)
        {
            BlockPlanks.EnumType type = getLeafType(blockState);

            switch (type)
            {
                case ACACIA:
                    itemToDrop = ItemRegistry.TWIG_ACACIA;
                    break;
                case BIRCH:
                    itemToDrop = ItemRegistry.TWIG_BIRCH;
                    break;
                case DARK_OAK:
                    itemToDrop = ItemRegistry.TWIG_DARK_OAK;
                    break;
                case JUNGLE:
                    itemToDrop = ItemRegistry.TWIG_JUNGLE;
                    break;
                case OAK:
                    itemToDrop = ItemRegistry.TWIG_OAK;
                    break;
                case SPRUCE:
                    itemToDrop = ItemRegistry.TWIG_SPRUCE;
            }
        }

        return itemToDrop;
    }
}

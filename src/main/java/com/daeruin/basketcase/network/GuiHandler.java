package com.daeruin.basketcase.network;

import com.daeruin.basketcase.client.gui.GuiBasket;
import com.daeruin.basketcase.inventory.ContainerBasket;
import com.daeruin.basketcase.inventory.ItemStackHandlerBasket;
import com.daeruin.basketcase.items.ItemBlockBasket;
import com.daeruin.basketcase.tileentity.TileEntityBasket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class GuiHandler implements IGuiHandler
{
    @Override
    public Object getServerGuiElement(int guiID, EntityPlayer player, World world, int x, int y, int z)
    {
        BlockPos pos = new BlockPos(x, y, z);
        TileEntity tileEntity = world.getTileEntity(pos);
        ItemStack heldItemStack = player.inventory.getCurrentItem();

        // We're opening the inventory from the block
        if (tileEntity != null && tileEntity instanceof TileEntityBasket)
        {
            IItemHandler basketInventory = tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            return new ContainerBasket(player.inventory, basketInventory, (TileEntityBasket) tileEntity, ((TileEntityBasket) tileEntity).numSlots);
        }
        // We're opening the inventory from the ItemStack
        else if (heldItemStack != ItemStack.EMPTY && heldItemStack.getItem() instanceof ItemBlockBasket)
        {
            IItemHandler basketInventory = heldItemStack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            if (basketInventory != null)
            {
                return new ContainerBasket(player.inventory, basketInventory, null, basketInventory.getSlots());
            }
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int guiID, EntityPlayer player, World world, int x, int y, int z)
    {
        BlockPos pos = new BlockPos(x, y, z);
        TileEntity tileEntity = world.getTileEntity(pos);
        ItemStack heldItemStack = player.inventory.getCurrentItem();

        // We're opening the inventory from the block
        if (tileEntity != null && tileEntity instanceof TileEntityBasket)
        {
            ItemStackHandlerBasket basketInventory = (ItemStackHandlerBasket) tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            return new GuiBasket(player.inventory, basketInventory, (TileEntityBasket) tileEntity);
        }
        // We're opening the inventory from the ItemStack
        else if (heldItemStack != ItemStack.EMPTY && heldItemStack.getItem() instanceof ItemBlockBasket)
        {
            ItemStackHandlerBasket basketInventory = (ItemStackHandlerBasket) heldItemStack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            if (basketInventory != null)
            {
                return new GuiBasket(player.inventory, basketInventory, null);
            }
        }

        return null;
    }
}

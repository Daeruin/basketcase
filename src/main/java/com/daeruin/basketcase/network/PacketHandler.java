package com.daeruin.basketcase.network;

import com.daeruin.basketcase.BasketCase;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketHandler
{
    public static final SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel(BasketCase.MODID);
    private static int packetIndex = 0;

    public static void registerPackets()
    {
        network.registerMessage(BasketInventoryUpdate.Handler.class, BasketInventoryUpdate.class, packetIndex++, Side.CLIENT);
    }
}

package com.daeruin.basketcase.network;

import com.daeruin.basketcase.BasketCase;
import com.daeruin.basketcase.inventory.ItemStackHandlerBasket;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.ParametersAreNonnullByDefault;

// Sends a basket's slot location and inventory in an NBT compound from the server to the client
public class BasketInventoryUpdate implements IMessage
{
    int slotIndex;
    NBTTagCompound nbt;

    @SuppressWarnings("unused")
    public BasketInventoryUpdate()
    {
    }

    public BasketInventoryUpdate(int slotIndex, NBTTagCompound nbt)
    {
        this.slotIndex = slotIndex;
        this.nbt = nbt;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.slotIndex = buf.readInt();
        this.nbt = ByteBufUtils.readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.slotIndex);
        ByteBufUtils.writeTag(buf, nbt);
    }

    // Send a message to the client - message has the slot index where a basket is located, along with the basket's inventory in NBT
    @ParametersAreNonnullByDefault
    public static class Handler implements IMessageHandler<BasketInventoryUpdate, IMessage>
    {
        @Override
        public IMessage onMessage(final BasketInventoryUpdate message, final MessageContext messageContext)
        {
            BasketCase.getProxy().getThreadListener(messageContext).addScheduledTask(() -> {
                final EntityPlayer player = BasketCase.getProxy().getPlayer(messageContext);

                if (player != null)
                {
                    // Get the basket ItemStack from the slot provided in the message, then get its inventory capability
                    ItemStack stack = player.openContainer.inventorySlots.get(message.slotIndex).getStack();
                    ItemStackHandlerBasket stackCapability = (ItemStackHandlerBasket) stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

                    if (stackCapability != null)
                    {
                        // Get data out of message's NBT and save to ItemStack's capability fields
                        stackCapability.deserializeNBT(message.nbt);
                    }
                }
            });

            return null;
        }
    }
}

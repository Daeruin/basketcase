package com.daeruin.basketcase.blocks;

import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.LinkedHashSet;

public final class BlockRegistry
{
    private static final Block BASKET_SMALL_GENERIC = new BlockTileEntityBasket("basket_small_generic", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_BARK_ACACIA = new BlockTileEntityBasket("basket_small_bark_acacia", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_BARK_BIRCH = new BlockTileEntityBasket("basket_small_bark_birch", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_BARK_DARK_OAK = new BlockTileEntityBasket("basket_small_bark_dark_oak", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_BARK_JUNGLE = new BlockTileEntityBasket("basket_small_bark_jungle", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_BARK_OAK = new BlockTileEntityBasket("basket_small_bark_oak", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_BARK_SPRUCE = new BlockTileEntityBasket("basket_small_bark_spruce", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_TWIG_ACACIA = new BlockTileEntityBasket("basket_small_twig_acacia", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_TWIG_BIRCH = new BlockTileEntityBasket("basket_small_twig_birch", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_TWIG_DARK_OAK = new BlockTileEntityBasket("basket_small_twig_dark_oak", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_TWIG_JUNGLE = new BlockTileEntityBasket("basket_small_twig_jungle", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_TWIG_OAK = new BlockTileEntityBasket("basket_small_twig_oak", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_SMALL_TWIG_SPRUCE = new BlockTileEntityBasket("basket_small_twig_spruce", BlockTileEntityBasket.BasketSize.SMALL);
    private static final Block BASKET_MEDIUM_GENERIC = new BlockTileEntityBasket("basket_medium_generic", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_BARK_ACACIA = new BlockTileEntityBasket("basket_medium_bark_acacia", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_BARK_BIRCH = new BlockTileEntityBasket("basket_medium_bark_birch", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_BARK_DARK_OAK = new BlockTileEntityBasket("basket_medium_bark_dark_oak", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_BARK_JUNGLE = new BlockTileEntityBasket("basket_medium_bark_jungle", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_BARK_OAK = new BlockTileEntityBasket("basket_medium_bark_oak", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_BARK_SPRUCE = new BlockTileEntityBasket("basket_medium_bark_spruce", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_TWIG_ACACIA = new BlockTileEntityBasket("basket_medium_twig_acacia", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_TWIG_BIRCH = new BlockTileEntityBasket("basket_medium_twig_birch", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_TWIG_DARK_OAK = new BlockTileEntityBasket("basket_medium_twig_dark_oak", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_TWIG_JUNGLE = new BlockTileEntityBasket("basket_medium_twig_jungle", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_TWIG_OAK = new BlockTileEntityBasket("basket_medium_twig_oak", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_MEDIUM_TWIG_SPRUCE = new BlockTileEntityBasket("basket_medium_twig_spruce", BlockTileEntityBasket.BasketSize.MEDIUM);
    private static final Block BASKET_LARGE_GENERIC = new BlockTileEntityBasket("basket_large_generic", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_BARK_ACACIA = new BlockTileEntityBasket("basket_large_bark_acacia", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_BARK_BIRCH = new BlockTileEntityBasket("basket_large_bark_birch", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_BARK_DARK_OAK = new BlockTileEntityBasket("basket_large_bark_dark_oak", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_BARK_JUNGLE = new BlockTileEntityBasket("basket_large_bark_jungle", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_BARK_OAK = new BlockTileEntityBasket("basket_large_bark_oak", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_BARK_SPRUCE = new BlockTileEntityBasket("basket_large_bark_spruce", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_TWIG_ACACIA = new BlockTileEntityBasket("basket_large_twig_acacia", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_TWIG_BIRCH = new BlockTileEntityBasket("basket_large_twig_birch", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_TWIG_DARK_OAK = new BlockTileEntityBasket("basket_large_twig_dark_oak", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_TWIG_JUNGLE = new BlockTileEntityBasket("basket_large_twig_jungle", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_TWIG_OAK = new BlockTileEntityBasket("basket_large_twig_oak", BlockTileEntityBasket.BasketSize.LARGE);
    private static final Block BASKET_LARGE_TWIG_SPRUCE = new BlockTileEntityBasket("basket_large_twig_spruce", BlockTileEntityBasket.BasketSize.LARGE);

    public static LinkedHashSet<Block> getBlocks()
    {
        // Using a LinkedHashSet to maintain the desired order in the creative tab
        LinkedHashSet<Block> BLOCKS = new LinkedHashSet<>();

        BLOCKS.add(BASKET_SMALL_GENERIC);
        BLOCKS.add(BASKET_MEDIUM_GENERIC);
        BLOCKS.add(BASKET_LARGE_GENERIC);
        BLOCKS.add(BASKET_SMALL_BARK_ACACIA);
        BLOCKS.add(BASKET_MEDIUM_BARK_ACACIA);
        BLOCKS.add(BASKET_LARGE_BARK_ACACIA);
        BLOCKS.add(BASKET_SMALL_BARK_BIRCH);
        BLOCKS.add(BASKET_MEDIUM_BARK_BIRCH);
        BLOCKS.add(BASKET_LARGE_BARK_BIRCH);
        BLOCKS.add(BASKET_SMALL_BARK_DARK_OAK);
        BLOCKS.add(BASKET_MEDIUM_BARK_DARK_OAK);
        BLOCKS.add(BASKET_LARGE_BARK_DARK_OAK);
        BLOCKS.add(BASKET_SMALL_BARK_JUNGLE);
        BLOCKS.add(BASKET_MEDIUM_BARK_JUNGLE);
        BLOCKS.add(BASKET_LARGE_BARK_JUNGLE);
        BLOCKS.add(BASKET_SMALL_BARK_OAK);
        BLOCKS.add(BASKET_MEDIUM_BARK_OAK);
        BLOCKS.add(BASKET_LARGE_BARK_OAK);
        BLOCKS.add(BASKET_SMALL_BARK_SPRUCE);
        BLOCKS.add(BASKET_MEDIUM_BARK_SPRUCE);
        BLOCKS.add(BASKET_LARGE_BARK_SPRUCE);
        BLOCKS.add(BASKET_SMALL_TWIG_ACACIA);
        BLOCKS.add(BASKET_MEDIUM_TWIG_ACACIA);
        BLOCKS.add(BASKET_LARGE_TWIG_ACACIA);
        BLOCKS.add(BASKET_SMALL_TWIG_BIRCH);
        BLOCKS.add(BASKET_MEDIUM_TWIG_BIRCH);
        BLOCKS.add(BASKET_LARGE_TWIG_BIRCH);
        BLOCKS.add(BASKET_SMALL_TWIG_DARK_OAK);
        BLOCKS.add(BASKET_MEDIUM_TWIG_DARK_OAK);
        BLOCKS.add(BASKET_LARGE_TWIG_DARK_OAK);
        BLOCKS.add(BASKET_SMALL_TWIG_JUNGLE);
        BLOCKS.add(BASKET_MEDIUM_TWIG_JUNGLE);
        BLOCKS.add(BASKET_LARGE_TWIG_JUNGLE);
        BLOCKS.add(BASKET_SMALL_TWIG_OAK);
        BLOCKS.add(BASKET_MEDIUM_TWIG_OAK);
        BLOCKS.add(BASKET_LARGE_TWIG_OAK);
        BLOCKS.add(BASKET_SMALL_TWIG_SPRUCE);
        BLOCKS.add(BASKET_MEDIUM_TWIG_SPRUCE);
        BLOCKS.add(BASKET_LARGE_TWIG_SPRUCE);

        return BLOCKS;
    }

    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event)
        {
            PrimalUtilReg.registerBlocks(event, getBlocks());
        }

        @SubscribeEvent
        public static void registerItemBlocks(RegistryEvent.Register<Item> event)
        {
            PrimalUtilReg.registerItemBlocks(event, getBlocks());
        }
    }
}

package com.daeruin.basketcase.blocks;

import com.daeruin.basketcase.BasketCase;
import com.daeruin.basketcase.config.BasketCaseConfig;
import com.daeruin.basketcase.inventory.ItemStackHandlerBasket;
import com.daeruin.basketcase.items.ItemBlockBasket;
import com.daeruin.basketcase.tileentity.TileEntityBasket;
import com.daeruin.primallib.IHasCustomItemBlock;
import com.daeruin.primallib.INeedsCustomModelReg;
import com.daeruin.primallib.blocks.BlockTileEntity;
import com.daeruin.primallib.util.PrimalUtilReg;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nonnull;

public class BlockTileEntityBasket extends BlockTileEntity implements IHasCustomItemBlock, INeedsCustomModelReg
{
    private final int numSlots;
    private final int maxStackSize;
    private final ResourceLocation guiTexture;
    private AxisAlignedBB AABB;

    BlockTileEntityBasket(String registryName, BasketSize basketSize)
    {
        // Material is cloth so baskets don't get destroyed by running water
        super(registryName, Material.CLOTH, 0.65F, true);
        this.setSoundType(SoundType.CLOTH);
        this.setHarvestLevel("axe", 0);
        this.numSlots = basketSize.numSlots; // Used in createTileEntity to initialize this value
        this.maxStackSize = basketSize.maxStackSize; // Used in createTileEntity to initialize this value
        this.guiTexture = new ResourceLocation("basketcase:textures/gui/basket.png");
        this.AABB = basketSize.AABB;
    }

    @Override
    public void registerCustomModel()
    {
        Item item = Item.getItemFromBlock(this);

        //noinspection ConstantConditions
        PrimalUtilReg.registerModel(item, 0, new ModelResourceLocation(item.getRegistryName().toString(), "normal"));
    }

    // These three getters are used by the ItemBlock's initCapabilities to attach the inventory to the ItemStack

    public int getNumSlots()
    {
        return numSlots;
    }

    public int getMaxStackSize()
    {
        return maxStackSize;
    }

    public ResourceLocation getGuiTexture()
    {
        return guiTexture;
    }

    @Override
    @Nonnull
    @SuppressWarnings("deprecation")
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return AABB;
    }

    @Override
    public Class getTileEntityClass()
    {
        return TileEntityBasket.class;
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    // Creates a new tile entity based on the numSlots, etc. of this block
    @Override
    public TileEntityBasket createTileEntity(World world, IBlockState state)
    {
        TileEntityBasket tileEntity = new TileEntityBasket();
        tileEntity.initializeTileEntity(getNumSlots(), getMaxStackSize(), getGuiTexture());
        return tileEntity;
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    // This prevents baskets from causing suffocation and removes heavy shadows around baskets
    @SuppressWarnings("deprecation")
    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    @Nonnull
    @SuppressWarnings("deprecation")
    public EnumBlockRenderType getRenderType(IBlockState state)
    {
        return EnumBlockRenderType.MODEL;
    }

    // Delay deletion of block until after harvestBlock, so inventory isn't deleted yet
    @Override
    public boolean removedByPlayer(@Nonnull IBlockState state, World world, @Nonnull BlockPos pos, @Nonnull EntityPlayer player, boolean willHarvest)
    {
        // If it will harvest, delay deletion of the block until after getDrops
        if (willHarvest)
            return true;
        //noinspection ConstantConditions
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }

    // Container is being broken, so spill contents
    // If sneaking, leave container as is
    // If not sneaking, destroy container
    @Override
    public void harvestBlock(@Nonnull World world, EntityPlayer player, @Nonnull BlockPos pos, @Nonnull IBlockState state, TileEntity tileEntity, ItemStack stack)
    {
        if (tileEntity != null)
        {
            IItemHandler inventory = tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            if (inventory != null && inventory instanceof ItemStackHandlerBasket)
            {
                spillInventoryContents(world, pos, tileEntity, inventory);
            }
        }

        world.setBlockToAir(pos); // Delete the block

        // If sneaking, spawn an empty basket
        if (player.isSneaking() || !BasketCaseConfig.hardcoreBasketBreakage)
        {
            spawnAsEntity(world, pos, new ItemStack(state.getBlock()));
        }
    }

    // Spill inventory contents into world by iterating through inventory
    private void spillInventoryContents(@Nonnull World world, @Nonnull BlockPos pos, TileEntity tileEntity, IItemHandler inventory)
    {
        for (int i = 0; i < inventory.getSlots(); i++)
        {
            ItemStack itemStackCopy = inventory.getStackInSlot(i);

            if (!itemStackCopy.isEmpty() && itemStackCopy.getCount() != 0)
            {
                tileEntity.getWorld().spawnEntity(new EntityItem(world, pos.getX(), pos.getY(), pos.getZ(), itemStackCopy));
            }
        }
    }

    // My custom method for picking up blocks by shift-right clicking on them
    // Called from onBlockActivated
    // Overrides BlockBase#pickUpBlock so I can sync inventory, then calls the super method for spawning and deleting block
    @Override
    public void pickUpBlock(World world, BlockPos pos, ItemStack itemStack)
    {
        TileEntity tileEntity = world.getTileEntity(pos);
        ItemStack stack = new ItemStack(this);

        if (tileEntity != null && tileEntity instanceof TileEntityBasket && !stack.isEmpty())
        {
            // Save TileEntity's unique data (numSlots, maxStackSize, guiTexture, coordinates, and ResourceLocation) to ItemStack

            // Feed new, empty NBT to TileEntity and write TileEntity's data to it
            NBTTagCompound tileEntityInnerNBT = new NBTTagCompound();
            tileEntityInnerNBT = tileEntity.writeToNBT(tileEntityInnerNBT);
            // Assign tile entity data to key BlockEntityTag - required by ItemBlock#setTileEntityNBT to set TileEntity's data when placing block
            NBTTagCompound blockEntityTagWrapper = new NBTTagCompound();
            blockEntityTagWrapper.setTag("BlockEntityTag", tileEntityInnerNBT);
            // Saves the TileEntity's NBT, in the BlockEntityTag wrapper, to the ItemStack's stackTagCompound
            stack.setTagCompound(blockEntityTagWrapper);

            // Save TileEntity's inventory to the ItemStack's inventory capability

            // Get the capability for the tile entity and item stack, and put the TileEntity's inventory into the stack
            // This allows us to view the inventory when the block is in an ItemStack in the player's inventory, and there's no TileEntity
            ItemStackHandlerBasket tileEntityCapability = (ItemStackHandlerBasket) tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            ItemStackHandlerBasket itemStackCapability = (ItemStackHandlerBasket) stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            if (tileEntityCapability != null && itemStackCapability != null)
            {
                // Serialize takes data from the ItemStack (or other object) and puts it into a new NBT, then returns the NBT
                NBTTagCompound inventoryNBT = tileEntityCapability.serializeNBT(); // Get the TileEntity capability's list of stacks and put them into an NBT tag
                itemStackCapability.deserializeNBT(inventoryNBT); // Write the stored stacks into the ItemStack capability's list of stacks
            }
        }

        super.pickUpBlock(world, pos, stack); // Spawn EntityItem and delete block
    }

    @Override
    public boolean useBlock(World world, BlockPos pos, EntityPlayer player)
    {
        if (!BasketCaseConfig.basketsAreDecorativeOnly)
        {
            // All information about the GUI is stored in the tile entity itself
            // The GuiHandler already has access to the tile entity
            player.openGui(BasketCase.INSTANCE, 0, world, pos.getX(), pos.getY(), pos.getZ());
        }

        return true;
    }

    // Update tile entity contents when placing the block
    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
        TileEntity tileEntity = world.getTileEntity(pos);

        if (tileEntity != null && tileEntity instanceof TileEntityBasket)
        {
            // Get the capability for the TileEntity and ItemStack, and put the stack's inventory data back into the TileEntity's capability
            // This is necessary because the ItemStack's inventory might have changed since the TileEntity's data was originally serialized into its NBT tag

            ItemStackHandlerBasket itemStackCapability = (ItemStackHandlerBasket) stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            ItemStackHandlerBasket tileEntityCapability = (ItemStackHandlerBasket) tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            if (tileEntityCapability != null && itemStackCapability != null)
            {
                NBTTagCompound inventoryNBT = itemStackCapability.serializeNBT(); // Get the ItemStack capability's list of stacks and put them into an NBT tag
                tileEntityCapability.deserializeNBT(inventoryNBT); // Write the stored stacks into the TileEntity capability's list of stacks
            }
        }
    }

    @Override
    public ItemBlock getItemBlock()
    {
        return new ItemBlockBasket(this);
    }

    public enum BasketSize
    {
        SMALL(4, BasketCaseConfig.maxStackSizeSmallBasket, new AxisAlignedBB(0.25F, 0.0F, 0.25F, 0.75F, 0.3125F, 0.75F)),
        MEDIUM(8, BasketCaseConfig.maxStackSizeMediumBasket, new AxisAlignedBB(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.5625F, 0.9375F)),
        LARGE(12, BasketCaseConfig.maxStackSizeLargeBasket, new AxisAlignedBB(0.0625F, 0.0F, 0.0625F, 0.9375F, 0.875F, 0.9375F));

        private final int numSlots;
        private final int maxStackSize;
        private final AxisAlignedBB AABB;

        BasketSize(int numSlots, int maxStackSize, AxisAlignedBB AABB)
        {
            this.numSlots = numSlots;
            this.maxStackSize = maxStackSize;
            this.AABB = AABB;
        }
    }
}
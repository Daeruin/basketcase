package com.daeruin.basketcase.config;

import com.daeruin.basketcase.BasketCase;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = BasketCase.MODID)
public class BasketCaseConfig
{
    private static final String LANG_KEY_PREFIX = "config." + BasketCase.MODID + ":";

    // Wood-specific baskets
    @Config.Name("Allow wood-specific baskets")
    @Config.LangKey(LANG_KEY_PREFIX + "allow_wood_specific_baskets")
    @Config.Comment("If true, each vanilla tree type will produce twigs and bark strips specific to that tree type, which allows you to craft colored baskets based on each tree type. If false, only the generic tan wicker will be available.")
    public static boolean allowWoodSpecificBaskets = true;
    // Basket sizes
    @Config.Name("Allow large baskets")
    @Config.LangKey(LANG_KEY_PREFIX + "allow_large_baskets")
    @Config.Comment("Whether large baskets should be craftable in the game.")
    public static boolean allowCraftingLargeBaskets = true;
    @Config.Name("Allow medium baskets")
    @Config.LangKey(LANG_KEY_PREFIX + "allow_medium_baskets")
    @Config.Comment("Whether medium baskets should be craftable in the game.")
    public static boolean allowCraftingMediumBaskets = true;
    @Config.Name("Allow small baskets")
    @Config.LangKey(LANG_KEY_PREFIX + "allow_small_baskets")
    @Config.Comment("Whether small baskets should be craftable in the game.")
    public static boolean allowCraftingSmallBaskets = true;
    // Basket behavior
    @Config.Name("Baskets are decorative only")
    @Config.LangKey(LANG_KEY_PREFIX + "baskets_are_decorative_only")
    @Config.Comment("Whether baskets should be purely decorative, with no inventory capabilities.")
    public static boolean basketsAreDecorativeOnly = false;
    @Config.Name("Baskets restricted to hot bar")
    @Config.LangKey(LANG_KEY_PREFIX + "baskets_restricted_to_hot_bar")
    @Config.Comment("Whether baskets should be restricted to the hot bar only. Set to false to allow baskets in the player's main inventory.")
    public static boolean basketsRestrictedToHotBar = true;
    @Config.Name("Baskets allowed in containers")
    @Config.LangKey(LANG_KEY_PREFIX + "baskets_allowed_in_containers")
    @Config.Comment("Whether baskets can be placed in other containers.")
    public static boolean basketsAllowedInContainers = false;
    @Config.Name("Hardcore basket breakage")
    @Config.LangKey(LANG_KEY_PREFIX + "hardcore_basket_breakage")
    @Config.Comment("If true, baskets are destroyed when broken unless the player is crouching. If false, baskets drop normally when broken.")
    public static boolean hardcoreBasketBreakage = false;
    // Max stack sizes
    @Config.Name("Max stack size for large basket")
    @Config.LangKey(LANG_KEY_PREFIX + "max_stack_size_for_large_basket")
    @Config.Comment({"How many items can be stacked inside an inventory slot in a large basket.",
            "Please note that this could result in buggy behavior if you change it while any large baskets exist in your world."})
    @Config.RangeInt(min = 0, max = 64)
    public static int maxStackSizeLargeBasket = 48;
    @Config.Name("Max stack size for medium basket")
    @Config.LangKey(LANG_KEY_PREFIX + "max_stack_size_for_medium_basket")
    @Config.Comment({"How many items can be stacked inside an inventory slot in a medium basket.",
            "Please note that this could result in buggy behavior if you change it while any medium baskets exist in your world."})
    @Config.RangeInt(min = 0, max = 64)
    public static int maxStackSizeMediumBasket = 32;
    @Config.Name("Max stack size for small basket")
    @Config.LangKey(LANG_KEY_PREFIX + "max_stack_size_for_small_basket")
    @Config.Comment({"How many items can be stacked inside an inventory slot in a small basket.",
            "Please note that this could result in buggy behavior if you change it while any small baskets exist in your world."})
    @Config.RangeInt(min = 0, max = 64)
    public static int maxStackSizeSmallBasket = 16;

    @Mod.EventBusSubscriber
    private static class ConfigEventHandler
    {
        @SubscribeEvent
        public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
        {
            if (event.getModID().equals(BasketCase.MODID))
            {
                ConfigManager.sync(BasketCase.MODID, Config.Type.INSTANCE);
            }
        }
    }
}

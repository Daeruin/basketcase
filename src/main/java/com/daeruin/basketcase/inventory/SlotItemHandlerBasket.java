package com.daeruin.basketcase.inventory;

import com.daeruin.basketcase.items.ItemBlockBasket;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;

public class SlotItemHandlerBasket extends SlotItemHandler
{
    SlotItemHandlerBasket(IItemHandler itemHandler, int index, int xPosition, int yPosition)
    {
        super(itemHandler, index, xPosition, yPosition);
    }

    // Don't allow baskets to be placed inside each other
    @Override
    public boolean isItemValid(@Nonnull ItemStack stack)
    {
        //noinspection SimplifiableIfStatement
        if (stack.isEmpty()
                || stack.getItem() instanceof ItemBlockBasket)
        {
            return false;
        }
        else
        {
            return super.isItemValid(stack);
        }
    }
}

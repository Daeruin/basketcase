package com.daeruin.basketcase.inventory;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IWorldNameable;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;

public class ItemStackHandlerBasket extends ItemStackHandler implements IWorldNameable
{
    private int maxStackSize;
    private ResourceLocation guiTexture;

    public ItemStackHandlerBasket(int numSlots, int maxStackSize, ResourceLocation guiTexture)
    {
        super(numSlots);

        this.maxStackSize = maxStackSize;
        this.guiTexture = guiTexture;

        // System.out.println("Item stack handler texture: " + this.guiTexture);
        // System.out.println("Item stack handler numSlots: " + this.maxStackSize);
    }

    // Default is 64 - override so it's based on the size passed in
    @Override
    public int getSlotLimit(int slot)
    {
        return this.maxStackSize;
    }

    // Used by GuiHandler to supply the guiTexture to the GUI when opening the inventory from the ItemStack
    public ResourceLocation getGuiTexture()
    {
        return guiTexture;
    }

    // Convert texture name into the unlocalized name of the basket block.
    // Returns a string that must be entered as a key into the lang file so it can be translated.
    // If I decide to put the basket size into the name, I could pass the block's registry name into
    // TileEntityBasket.initializeTileEntity when called from BlockTileEntityBasket.createTileEntity,
    // and from there into the ItemStackHandlerBasket constructor.
    @Nonnull
    @Override
    public String getName()
    {
        // System.out.println("Texture: " + this.guiTexture);
        // System.out.println("Path: " + this.guiTexture.getPath());
        // System.out.println("Substring: " + this.guiTexture.getPath().substring(13));
        // System.out.println("Replace: " + this.guiTexture.getPath().substring(13).replace(".png", ""));
        // String name = this.guiTexture.getPath().substring(13).replace(".png", "");
        // name = "container." + name;
        // return name;
        return "container.basket";
    }

    @Override
    public boolean hasCustomName()
    {
        return false;
    }

    // Get the translated name of this inventory
    @Nonnull
    @Override
    public ITextComponent getDisplayName()
    {
        return new TextComponentTranslation(this.getName());
    }
}

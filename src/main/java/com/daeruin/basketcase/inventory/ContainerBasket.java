package com.daeruin.basketcase.inventory;

import com.daeruin.basketcase.tileentity.TileEntityBasket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nonnull;

public class ContainerBasket extends Container
{
    private static final int NUM_ROWS = 2; // numSlots must be divisible by this
    private static final int X_CENTER = 176 / 2; // 176 is the width of the basket GUI - see GuiBasket.xSize
    private static final int Y_START = 17;
    private static final int SLOT_SIZE = 18;
    private final TileEntityBasket tileEntityBasket;
    private final int numSlots;

    public ContainerBasket(IInventory playerInventory, IItemHandler basketInventory, TileEntityBasket tileEntityBasket, int numSlots)
    {
        this.tileEntityBasket = tileEntityBasket;
        this.numSlots = numSlots;
        int numColumns = numSlots / NUM_ROWS;
        int xStart = X_CENTER - (numColumns * SLOT_SIZE) / 2;

        // Container inventory
        for (int y = 0; y < NUM_ROWS; ++y)
        {
            for (int x = 0; x < numColumns; ++x)
            {
                int slotIndex = x + (y * numColumns);
                int xPosition = xStart + (x * SLOT_SIZE);
                int yPosition = Y_START + (y * SLOT_SIZE);

                this.addSlotToContainer(new SlotItemHandlerBasket(basketInventory, slotIndex, xPosition, yPosition));
            }
        }

        // Player inventory
        for (int y = 0; y < 3; ++y)
        {
            for (int x = 0; x < 9; ++x)
            {
                this.addSlotToContainer(new Slot(playerInventory, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
                // x + y * 9 + 9
                // 0 + 0 * 9 + 9 = 9
                // 1 + 0 * 9 + 9 = 10
                // 2 + 0 * 9 + 9 = 11
                // 3 + 0 * 9 + 9 = 12
                // 4 + 0 * 9 + 9 = 13
                // 5 + 0 * 9 + 9 = 14
                // 6 + 0 * 9 + 9 = 15
                // 7 + 0 * 9 + 9 = 16
                // 8 + 0 * 9 + 9 = 17
                // 0 + 1 * 9 + 9 = 18
                // 1 + 1 * 9 + 9 = 19
                // 2 + 1 * 9 + 9 = 20
                // 3 + 1 * 9 + 9 = 21
                // 4 + 1 * 9 + 9 = 22
                // 5 + 1 * 9 + 9 = 23
                // 6 + 1 * 9 + 9 = 24
                // 7 + 1 * 9 + 9 = 25
                // 8 + 1 * 9 + 9 = 26
                // ...
            }
        }

        // Hot bar
        for (int x = 0; x < 9; ++x)
        {
            this.addSlotToContainer(new Slot(playerInventory, x, 8 + x * 18, 142));
        }
    }

    // public IItemHandler getBasketInventory()
    // {
    //     return basketInventory;
    // }

    // Will close the container if it becomes unavailable, e.g. if the basket is broken
    @Override
    public boolean canInteractWith(@Nonnull EntityPlayer playerIn)
    {
        // If TileEntity is null, we're opening the basket from the ItemStack
        return this.tileEntityBasket == null || this.tileEntityBasket.isUsableByPlayer(playerIn);
    }

    // @Override
    // public void onContainerClosed(EntityPlayer playerIn)
    // {
    //     this.detectAndSendChanges();
    //     // playerIn.inventory.markDirty();
    //     // playerIn.inventoryContainer.detectAndSendChanges();
    //     // this.tileEntityBasket.getUpdatePacket();
    //     // System.out.println(this.tileEntityBasket);
    //     super.onContainerClosed(playerIn);
    // }
    //
    // @Nonnull
    // @Override
    // public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player)
    // {
    //     // System.out.println("slotClick");
    //     this.detectAndSendChanges();
    //     return super.slotClick(slotId, dragType, clickTypeIn, player);
    // }

    /**
     * Merges provided ItemStack with the first available one in the container/player inventor between minIndex
     * (included) and maxIndex (excluded).
     */
    @Override
    protected boolean mergeItemStack(ItemStack sourceStack, int startIndex, int endIndex, boolean useEndIndex)
    {
        boolean success = false;
        int index = startIndex;

        if (useEndIndex)
            index = endIndex - 1;

        Slot targetSlot;
        ItemStack targetStack;

        // Try to add to partial stacks of the same item
        if (sourceStack.isStackable())
        {
            while (sourceStack.getCount() > 0 && (!useEndIndex && index < endIndex || useEndIndex && index >= startIndex))
            {
                targetSlot = this.inventorySlots.get(index);
                targetStack = targetSlot.getStack();

                // If source item and target item are the same
                // Then figure out if there's room to move items to the target slot
                if (!targetStack.isEmpty()
                        && targetStack.getItem() == sourceStack.getItem()
                        && (!sourceStack.getHasSubtypes() || sourceStack.getMetadata() == targetStack.getMetadata())
                        && ItemStack.areItemStackTagsEqual(sourceStack, targetStack))
                {
                    int combinedSize = targetStack.getCount() + sourceStack.getCount();
                    int maxTargetSize = Math.min(targetStack.getMaxStackSize(), targetSlot.getSlotStackLimit());

                    if (combinedSize <= maxTargetSize) // Both stacks can fit in target slot
                    {
                        sourceStack.setCount(0);
                        targetStack.setCount(combinedSize);
                        targetSlot.onSlotChanged();
                        success = true;
                    }
                    else if (targetStack.getCount() < maxTargetSize) // Can't fit everything, but can fit some
                    {
                        sourceStack.shrink(maxTargetSize - targetStack.getCount());
                        targetStack.setCount(maxTargetSize);
                        targetSlot.onSlotChanged();
                        success = true;
                    }
                }

                if (useEndIndex)
                    --index;
                else
                    ++index;
            }
        }

        // Try to put into any empty slots
        if (sourceStack.getCount() > 0)
        {
            if (useEndIndex)
                index = endIndex - 1;
            else
                index = startIndex;

            while (sourceStack.getCount() > 0 && (!useEndIndex && index < endIndex || useEndIndex && index >= startIndex))
            {
                targetSlot = this.inventorySlots.get(index);
                targetStack = targetSlot.getStack();

                // Forge: Make sure to respect isItemValid in the slot.
                if (targetStack.isEmpty() && targetSlot.isItemValid(sourceStack))
                {
                    if (sourceStack.getCount() <= targetSlot.getItemStackLimit(sourceStack))
                    {
                        targetSlot.putStack(sourceStack.copy());
                        targetSlot.onSlotChanged();
                        sourceStack.setCount(0);
                        success = true;
                        break;
                    }
                    else
                    {
                        ItemStack newStack = sourceStack.copy();
                        newStack.setCount(targetSlot.getItemStackLimit(sourceStack));
                        targetSlot.putStack(newStack);
                        targetSlot.onSlotChanged();
                        sourceStack.shrink(targetSlot.getItemStackLimit(sourceStack));
                        success = true;
                    }
                }

                if (useEndIndex)
                {
                    --index;
                }
                else
                {
                    ++index;
                }
            }
        }

        return success;
    }

    @Override
    @Nonnull
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int fromSlotIndex)
    {
        ItemStack previous = ItemStack.EMPTY;
        Slot fromSlot = this.inventorySlots.get(fromSlotIndex);
        final int
                CONTAINER_START = 0,
                CONTAINER_END = this.numSlots - 1,
                PLAYER_INV_START = CONTAINER_END + 1,
                PLAYER_INV_END = PLAYER_INV_START + 26,
                HOT_BAR_START = PLAYER_INV_END + 1,
                HOT_BAR_END = HOT_BAR_START + 8;

        if (fromSlot != null && fromSlot.getHasStack())
        {
            ItemStack current = fromSlot.getStack();
            previous = current.copy();

            // Source item is in the container
            if (fromSlotIndex <= CONTAINER_END)
            {
                // Transfer from container inventory to player inventory
                if (!this.mergeItemStack(current, PLAYER_INV_START, HOT_BAR_END + 1, true))
                {
                    return ItemStack.EMPTY;
                }
            }
            else
            {
                // Transfer from player inventory to container inventory
                if (!this.mergeItemStack(current, CONTAINER_START, CONTAINER_END + 1, false))
                {
                    return ItemStack.EMPTY;
                }
            }

            if (current.getCount() == 0)
            {
                fromSlot.putStack(ItemStack.EMPTY);
            }
            else
            {
                fromSlot.onSlotChanged();
            }

            if (current.getCount() == previous.getCount())
            {
                return ItemStack.EMPTY;
            }

            fromSlot.onTake(playerIn, current);
        }

        return previous;
    }
}

package com.daeruin.basketcase.inventory;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

// Used by the basket ItemStack to maintain its inventory

// Originally I was implementing ICapabilityProvider
// However, this failed to serialize the NBT when making an ItemStack copy
// This prevented the inventory from persisting when the ItemStack was picked up in the player's inventory, put into a chest, or thrown on the ground
// For some reason, implementing ICapabilitySerializable and farming out the serialization/deserialization to the capability's storage handler works

// Before that, I had tried to implement ICapabilityProvider on ItemStackHandlerBasket directly
// That won't work with ICapabilitySerializable, because the serialization/deserialization methods clash with INBTSerializable on the superclass ItemStackHandler

public class BasketInventoryProvider implements ICapabilitySerializable
{
    private final ItemStackHandlerBasket inventory;

    public BasketInventoryProvider(int numSlots, int maxStackSize, ResourceLocation guiTexture)
    {
        inventory = new ItemStackHandlerBasket(numSlots, maxStackSize, guiTexture);
    }

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing)
    {
        //noinspection unchecked
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T) inventory : null;
    }

    // Farm out the serialization/deserialization to the capability's storage handler
    @Override
    public NBTBase serializeNBT()
    {
        return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.getStorage().writeNBT(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, inventory, null);
    }

    // Farm out the serialization/deserialization to the capability's storage handler
    @Override
    public void deserializeNBT(NBTBase nbt)
    {
        CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.getStorage().readNBT(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, inventory, null, nbt);
    }
}
